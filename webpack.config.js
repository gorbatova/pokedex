/* eslint-disable no-undef */
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const htmlPlugin = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: 'index.html',
  hash: true
});

module.exports = {
  entry: path.resolve(__dirname, 'src'),
  resolve: {
    alias: {
      Comps: path.resolve(__dirname, './src/components/'),
      Container: path.resolve(__dirname, './src/components/container/'),
      Presentational: path.resolve(__dirname, './src/components/presentational/'),
      CONST: path.resolve(__dirname, './src/constants')
    },
    extensions: [ '.jsx', '.js' ]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/g,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          }
        ]
      }
    ]
  },

  plugins: [htmlPlugin]
}