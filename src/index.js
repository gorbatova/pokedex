import React from 'react'
import ReactDOM from 'react-dom'
import Pokedex from './Pokedex.jsx'

const wrapper = document.getElementById( 'pokedex' )
wrapper ? ReactDOM.render( <Pokedex/>, wrapper ) : false